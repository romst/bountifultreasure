local entities = require "SynchronyExtendedAPI.extended.Entities"
local utils = require "SynchronyExtendedAPI.utils.APIUtils"
local debugConsole = require "SynchronyExtendedAPI.utils.DebugConsole"

local ecs = require "system.game.Entities"

local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local inventory = require "necro.game.item.Inventory"
local currency = require "necro.game.item.Currency"
local action = require "necro.game.system.Action"
local player = require "necro.game.character.Player"
local collision = require "necro.game.tile.Collision"
local move = require "necro.game.system.Move"

local components = require "necro.game.data.Components"
local field = components.field

components.register {
        thrown = {
            field.bool("active", false),
        },
        goldModification = {
            field.bool("calculating", false),
        },
}

entities.modifyAllPlayers(
    "initGold",
    function (ev)
        ev.entity.BountyfulTreasures_goldModification = {}
    end)

entities.modifyAllItems(
    "thrown",
    function (ev)
        if ev.entity.weaponThrowable ~= nil then
            ev.entity.BountyfulTreasures_thrown = { active = false }
        end
    end)

local function shouldDuplicate(item)
    return not (item:hasComponent("BountyfulTreasures_thrown") and item.BountyfulTreasures_thrown.active)
       and not item:hasComponent("consumable")
       and not item:hasComponent("spell")
       and not string.find(ecs.getEntityTypeName(item), "_")
end

utils.safeAddEvent(
    event.inventoryAddItem,
    "destroyPreviousItem",
    {
        order = "dropSlot",
        sequence = -1,
        filter = "itemSlot"
    }, function (ev)
    local slot = ev.item.itemSlot.name

    if slot == "weapon"
        or slot == "shovel"
        or slot == "head"
        or slot == "armor"
        or slot == "feet"
        or slot == "ring"
        or slot == "torch"
    then
        local relevantItem =
                ev.holder.id == nil
                or (ev.holder:hasComponent("controllable") and ev.holder.controllable.playerID ~= 0)

        if not relevantItem then
            return
        end

        -- remove previous item, as the floor item will be duplicated
        local item = inventory.getItemInSlot(ev.holder, slot, 1)
        if item then
            if shouldDuplicate(item) then
                inventory.destroy(item)
            else
                inventory.drop(item)
                object.moveToNearbyVacantTile(item, collision.Type.ITEM, move.Type.NONE, 5)
            end
        end
        if shouldDuplicate(ev.item) then
            object.spawn(ecs.getEntityTypeName(ev.item), ev.item.position.x, ev.item.position.y)
        end
    end
end)

utils.safeAddEvent(
    event.weaponAttack,
    "markThrownWeapon",
    {
        order = "throw",
        filter = { "weaponThrowable", "BountyfulTreasures_thrown" },
        sequence = -1,
    }, function (ev)
    if ev.weapon.weaponThrowable.active and action.isDirection(ev.direction) then
		ev.weapon.BountyfulTreasures_thrown.active = true
	end
end)

-- shared gold

utils.safeAddEvent(
    event.inventoryCollectItem,
    "shareCurrency",
    {
        order = "currency",
        sequence = 1,
        filter = {"itemStack", "itemSlot", "itemCurrency"},
    },
    function (ev)
        -- debugConsole.printDebug(ev)
        debugConsole.printDebug(ev.holder.id .. ": " .. ev.itemStack.itemStack.quantity .. " + " .. ev.item.itemStack.quantity)
    end)

utils.safeAddEvent(
    event.objectCurrency,
    "shareCurrency",
    {
        order = "damage",
        sequence = 1,
        filter = "BountyfulTreasures_goldModification"
    },
    function (ev)
        -- debugConsole.printDebug(ev.entity.id .. ": " .. ev.oldAmount .. " -> " .. ev.newAmount .. " | " .. ev.difference)
        -- utils.safeCall(function()
        --     if ev.currency == "gold" and not ev.entity.BountyfulTreasures_goldModification.calculating then
        --         for _, player in ipairs(player.getPlayerEntities()) do
        --             if player:hasComponent("BountyfulTreasures_goldModification") then
        --                 player.BountyfulTreasures_goldModification.calculating = true
        --             end
        --         end
        --         for _, player in ipairs(player.getPlayerEntities()) do
        --             if player:hasComponent("BountyfulTreasures_goldModification") and player.id ~= ev.entity.id then
        --                 local gold = inventory.getItemInSlot(player, "gold", 1)
        --                 local purse = inventory.getItemsInSlot(player, "gold")

        --                 if gold then
        --                     gold.itemStack.quantity = ev.oldAmount
        --                     debugConsole.printDebug("Set " .. player.id .. ": " .. ev.oldAmount)
        --                 else
        --                     currency.add(player, currency.Type.GOLD, ev.difference)
        --                 end
        --             end
        --         end
        --         for _, player in ipairs(player.getPlayerEntities()) do
        --             if player:hasComponent("BountyfulTreasures_goldModification") then
        --                 player.BountyfulTreasures_goldModification.calculating = false
        --             end
        --         end
        --     end
        -- end)
    end)